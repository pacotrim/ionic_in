import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, App, LoadingController } from 'ionic-angular';
import { SessionsProvider } from '../../providers/sessions/sessions';
import { LoginPage } from '../login/login';
import { PostsProvider } from '../../providers/posts/posts';
import { PostModel } from '../../models/post.model';
import { UserModel } from '../../models/user.model';

/**
 * Generated class for the FeedPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-feed',
  templateUrl: 'feed.html',
})
export class FeedPage {

  publicacoes: PostModel[] = [];
  page: number = 1;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public sessions: SessionsProvider,
    public app: App,
    public posts: PostsProvider,
    public loadingCtrl: LoadingController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad FeedPage');
  }

  ionViewWillEnter(){
    this.getPosts();
  }

  logout(): void{
    this.sessions.logout();
    this.app.getRootNav().push(LoginPage);
  }

  doRefresh(refresher) {
    this.page = 1;

    let user = JSON.parse(localStorage.getItem("user"));
    this.posts.getPosts(user.authentication_token, this.page.toString()).subscribe(
      (data) => {
        this.publicacoes = data.json().posts;
        refresher.complete();
      }, (error) => {
        console.log(error.json());
        refresher.complete();
      }
    );
  }

  doInfinite(infiniteScroll){
    let user= JSON.parse(localStorage.getItem("user"));
    this.page +=1;
    this.posts.getPosts(user.authentication_token, this.page.toString()).subscribe(
      (data)=>{
        this.publicacoes = this.publicacoes.concat(data.json().posts);
        infiniteScroll.complete();
      },
      (error)=> {
        infiniteScroll.complete();
      }
    );
  }

  getPosts(page?: number){
    const loader = this.loadingCtrl.create({
      content: "Marca ae..."
    });
    loader.present()
    let user= JSON.parse(localStorage.getItem("user"));
    if(page){
      this.page=page;
    }
    this.posts.getPosts(user.authentication_token, this.page.toString()).subscribe(
      (data)=>{
        console.log(data.json());
        loader.dismiss();
        this.publicacoes=data.json().posts;
      },
      (error)=>{
        loader.dismiss();
        console.log(error.json());
      }
    );
  }

}
