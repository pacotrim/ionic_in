import { Component } from '@angular/core';

import { AboutPage } from '../about/about';
import { ContactPage } from '../contact/contact';
import { NewPostPage } from '../new-post/new-post';
import { FeedPage } from '../feed/feed';

@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {

  tab1Root = NewPostPage;
  tab2Root = FeedPage;
  tab3Root = ContactPage;

  constructor() {
  }
}
