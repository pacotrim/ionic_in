import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Http } from '../../../node_modules/@angular/http';
import { PostModel } from '../../models/post.model';

/*
  Generated class for the PostsProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class PostsProvider {

  apiUrl = '/api';

  constructor(public http: Http) {
    console.log('Hello PostsProvider Provider');
  }

  postar(post: PostModel, token: string){
    return this.http.post(`${this.apiUrl}/posts`, { post: post, authentication_token: token });
  }

  getPosts(token: string, page: string){
    return this.http.get(`${this.apiUrl}/posts?authentication_token=${token}&page=${page}`)
  }

}
