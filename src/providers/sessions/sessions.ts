import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Http } from '../../../node_modules/@angular/http';


/*
  Generated class for the SessionsProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class SessionsProvider {

  apiUrl='/api';

  constructor(public http: Http) {
  }

  login(username: string, password: string){
    return this.http.post(`${this.apiUrl}/login`, { email: username , password: password})
  }

  logout():void{
    localStorage.removeItem("logado");
  }

}
