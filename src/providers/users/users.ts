import { Injectable } from '@angular/core';
import { Http } from '../../../node_modules/@angular/http';
import { UserModel } from '../../models/user.model';

/*
  Generated class for the UsersProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class UsersProvider {

  apiUrl = "/api";

  constructor(public http: Http) {
  }

  create(user: UserModel){
    return this.http.post(`${this.apiUrl}/users`, { user: user});

  }
}
